#ifndef CPath_h__
#define CPath_h__



class CEnumPath
{
public:
	~CEnumPath();
	CEnumPath(const wchar_t* pPath,BOOL bDir = TRUE,BOOL bFullPath = FALSE);
	wchar_t* operator[](int n);
	
	int GetItemCount();
	
private:
	int m_nCount;
	wchar_t* m_Path[MAX_PATH_XC];
};


#endif // CPath_h__